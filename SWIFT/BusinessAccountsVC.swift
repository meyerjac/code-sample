//
//  BusinessAccountsVC.swift
//  SpeechFeast
//
//  Created by Jackson Meyer on 1/21/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import UIKit
import DropDown
import Firebase

class BusinessAccountsVC: UIViewController, UITextFieldDelegate {
    
    //MARK: - OUTLETS
    @IBOutlet weak var FullNameTextField: UITextField!
    @IBOutlet weak var PhoneNumberTextField: UITextField!
    @IBOutlet weak var BusinessEmailTextField: UITextField!
    @IBOutlet weak var BusinessNameTextField: UITextField!
    @IBOutlet weak var BusinessAddressTextField: UITextField!
    @IBOutlet weak var CountryTextField: UITextField!
    @IBOutlet weak var StateTextField: UITextField!
    @IBOutlet weak var CityTextField: UITextField!
    @IBOutlet weak var PostalCodeTextField: UITextField!
    @IBOutlet weak var PhoneCountryFlagImage: UIImageView!
    @IBOutlet weak var PhoneDownArrowImage: UIImageView!
    @IBOutlet weak var PhoneCodeLabel: UILabel!
    @IBOutlet weak var TermsOfServiceLabel: UILabel!
    @IBOutlet weak var TermsOfServiceCheckboxButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBAction func TermsOfServiceCheckboxButtonClicked(_ sender: UIButton) {
        if didAcceptTerms {
            TermsOfServiceCheckboxButton.isSelected = false
            didAcceptTerms = false
        } else {
            TermsOfServiceCheckboxButton.isSelected = true
            didAcceptTerms = true
        }
    }
    @IBAction func ContinueButtonClicked(_ sender: UIButton) {
        if allFieldsAreValidated() {
            let company = Company(owner_name: FullNameTextField.text!, owner_phone_number: PhoneNumberTextField.text!.formattedPhoneNumber(phone: PhoneNumberTextField.text!), business_email: BusinessEmailTextField.text!, business_legal_name:  BusinessNameTextField.text!, business_address: BusinessAddressTextField.text!, business_country: CountryTextField.text!, business_state: StateTextField.text!, business_city: CityTextField.text!, business_zip: Int(PostalCodeTextField.text!)!)
            
            let companyDetailObject = company.toAnyObject()
            
            if let uid = Auth.auth().currentUser?.uid {
                let companyDetailsRef = Database.database().reference().child("COMPANY/\(uid)")
                companyDetailsRef.setValue(companyDetailObject) { (error, ref) -> Void in
                    if error == nil {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navController = storyboard.instantiateViewController(withIdentifier: "mainNavigationController")
                        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                        appDelegate.window?.rootViewController = navController
                    }
                }
            }
        }
    }
    
    //MARK: VARIABLES
    var didAcceptTerms: Bool = false
    let choosePhoneCodeDropDown = DropDown()
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    var textFields = [UITextField]()
    var invalidTextField: UITextField?

    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFields = [FullNameTextField, PhoneNumberTextField, BusinessEmailTextField, BusinessNameTextField, BusinessAddressTextField, CountryTextField, CityTextField, StateTextField, PostalCodeTextField]
        setupDropDowns()
        setGestureRecognizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addKeyboardObservers()
    }
    
    //MARK: - KEYBOARD
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.tintColor = UIColor(red:0.35, green:0.93, blue:0.80, alpha:1.0)
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        FullNameTextField.inputAccessoryView = keyboardToolbar
        PhoneNumberTextField.inputAccessoryView = keyboardToolbar
        BusinessEmailTextField.inputAccessoryView = keyboardToolbar
        BusinessNameTextField.inputAccessoryView = keyboardToolbar
        BusinessAddressTextField.inputAccessoryView = keyboardToolbar
        CityTextField.inputAccessoryView = keyboardToolbar
        PostalCodeTextField.inputAccessoryView = keyboardToolbar
    }
    
    func addKeyboardObservers() {
        setDoneOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountsVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountsVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardFrame = keyboardFrame.cgRectValue
            
            var contentInset:UIEdgeInsets = self.scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height + 8
            scrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let field = invalidTextField {
            if field == textField {
                //field that was selected was invalid so remove red border
                field.layer.borderWidth = 0
                field.borderStyle = UITextField.BorderStyle.none
            }
        }
    }
    
    //SHOW DROP DOWNS OR KEYBOARD
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let field = invalidTextField {
            if field == textField {
                //field that was selected was invalid so remove red border
                field.layer.borderWidth = 0
                field.borderStyle = UITextField.BorderStyle.none
            }
        }
        
        if textField.tag == 2 {
            view.endEditing(true)
            countryDropDown.show()
            return false
        } else if textField.tag == 3 {
            view.endEditing(true)
            stateDropDown.show()
            return false
        } else {
            return true
        }
    }
    
    @objc func tapped(recognizer: UIGestureRecognizer) {
        choosePhoneCodeDropDown.show()
    }
    
    func setGestureRecognizers() {
        let Tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        PhoneDownArrowImage.addGestureRecognizer(Tap)
        PhoneCountryFlagImage.addGestureRecognizer(Tap)
        
        //setUp T&C
        TermsOfServiceLabel.text = "By checking this box, you agree to our Terms & Conditions and Privacy Policy and are acting within you authority on behalf of this company."
        let text = (TermsOfServiceLabel.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms & Conditions")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
        TermsOfServiceLabel.attributedText = underlineAttriString
        
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(termsOfServiceTapped))
        self.TermsOfServiceLabel.addGestureRecognizer(mytapGestureRecognizer)
    }

    func setupDropDowns() {
        //Setup DropDowns
        setupPhoneCodeDropDown()
        setupCountryDropDown()
        setupStateDropDown()
        
        let appearance = DropDown.appearance()
        appearance.cellHeight = 36
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        appearance.selectionBackgroundColor = UIColor(red:0.35, green:0.93, blue:0.80, alpha:1.0)
    }
    
    func setupPhoneCodeDropDown() {
        choosePhoneCodeDropDown.anchorView = PhoneNumberTextField
        choosePhoneCodeDropDown.dataSource = ["United States", "Canada", "Mexico", "Italy", "France"]
        choosePhoneCodeDropDown.direction = .bottom
        
       choosePhoneCodeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
        }
    }
    
    func setupCountryDropDown() {
        countryDropDown.anchorView = CountryTextField
        countryDropDown.dataSource = Constants.countries
        countryDropDown.direction = .bottom
        
       countryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.CountryTextField.text = item
        }
    }
    
    func setupStateDropDown() {
        stateDropDown.anchorView = StateTextField
        stateDropDown.dataSource = Constants.states
        stateDropDown.direction = .top
        
        stateDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.StateTextField.text = item
        }
    }
    
    func allFieldsAreValidated() -> Bool{
        for n in 0...textFields.count-1 {
           if n == 8 {
                //zip code
                if textFields[n].text?.count != 5 {
                    highlightInvalidTextField(field: textFields[n])
                    return false
                }
            } else if n == 1 {
                //phone number
                if textFields[n].text?.getwesternArabicNumeralsOnly() != 10 {
                    highlightInvalidTextField(field: textFields[n])
                    return false
                }
            } else {
                //all other fields
                if textFields[n].text?.count ?? 0 < 1 {
                    highlightInvalidTextField(field: textFields[n])
                    return false
                }
            }
        }
        
        if !(isValidEmail(testStr: BusinessEmailTextField.text!)) {
             highlightInvalidTextField(field: BusinessEmailTextField)
        }
        
        if TermsOfServiceCheckboxButton.isSelected {
            return true
        } else {
            
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.TermsOfServiceLabel.center.x - 5, y: self.TermsOfServiceLabel.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.TermsOfServiceLabel.center.x + 5, y: self.TermsOfServiceLabel.center.y))
            
            self.TermsOfServiceLabel.layer.add(animation, forKey: "position")
            
            return false
        }
    }
    
    func highlightInvalidTextField(field: UITextField) {
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.red.cgColor
        field.borderStyle = UITextField.BorderStyle.roundedRect
        invalidTextField = field
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @objc func termsOfServiceTapped(recognizer: UITapGestureRecognizer) {
        let text = (TermsOfServiceLabel.text)!
        let termsRange = (text as NSString).range(of: "Terms & Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
//          THIS ISN"T WORKING IN SWIFT 4.2
//
//        if recognizer.didTapAttributedTextInLabel(label: TermsOfServiceLabel, inRange: termsRange) {
//
//        } else if recognizer.didTapAttributedTextInLabel(label: TermsOfServiceLabel, inRange: privacyRange) {
//
//        } else {
//            print("Tapped none")
//        }
    }
}

extension String {
    func getwesternArabicNumeralsOnly() -> Int {
        let pattern = UnicodeScalar("0")..."9"
        let charOfNum = unicodeScalars
        .compactMap { pattern ~= $0 ? Character($0) : nil }
        return charOfNum.count
    }
}

extension String {
    func formattedPhoneNumber(phone: String) -> Int {
        let pattern = UnicodeScalar("0")..."9"
        let charOfNum = unicodeScalars
            .compactMap { pattern ~= $0 ? Character($0) : nil }
        if charOfNum.count == 0 { return 0}
        
        let newString = charOfNum.map { String($0) }
            .joined(separator: "")
        
        return Int(newString)!
    }
}
